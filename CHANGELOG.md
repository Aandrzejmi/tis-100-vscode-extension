# Change Log

All notable changes to the "tis-100-language" extension will be documented in this file.

## 0.0.1 - 13.10.2020
Test launch

## 0.1.1 - 13.10.2020
- Highlight of comments with ":" mark fixed
- Marketplace info updated

## 0.1.2 - 14.10.2020
- Color of breakpoint sign
- Undocumented opcode support fixed 